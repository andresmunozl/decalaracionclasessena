export class Carro {
    private modelo: number
    private placa: string
    private marca: string
    private puertas: number
    private color: string


    constructor(modelo: number, placa: string, puertas: number, marca: string, color: string) {
        this.modelo = modelo
        this.placa = placa
        this.marca = marca
        this.puertas = puertas
        this.color = color
    }

    public getModelo() {
        return this.modelo
    }

    public getPlaca() {
        return this.placa
    }

    public setModelo(modelo: number) {
        this.modelo = modelo
    }

    public setPlaca(placa: string) {
        this.placa = placa
    }

    public encender() {
        console.log("el carro encendió");
    }

    public desplazarCarro() {
        console.log("el carro está en movimiento!");

    }
}
